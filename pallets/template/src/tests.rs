use crate::mock::*;
use frame_support::assert_ok;

#[test]
fn it_works() {
	new_test_ext().execute_with(|| {
		let mut call = Box::new(Call::TemplateModule(pallet_template::Call::<Test>::add_club {
			club_id: 1,
			member: 2
		}));
		assert_ok!(Sudo::sudo(Origin::signed(1), call));
		call = Box::new(Call::TemplateModule(pallet_template::Call::<Test>::rm_club {
			club_id: 1,
			member: 2
		}));
		assert_ok!(Sudo::sudo(Origin::signed(1), call));
	});
}
