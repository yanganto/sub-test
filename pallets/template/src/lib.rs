#![cfg_attr(not(feature = "std"), no_std)]

/// Edit this file to define custom logic or remove it if it is not needed.
/// Learn more about FRAME and the core library of Substrate FRAME pallets:
/// <https://docs.substrate.io/v3/runtime/frame>
pub use pallet::*;

#[cfg(test)]
mod mock;

#[cfg(test)]
mod tests;

#[frame_support::pallet]
pub mod pallet {
	use frame_support::{dispatch::DispatchResult, pallet_prelude::*};
	use frame_system::pallet_prelude::*;

	/// Configure the pallet by specifying the parameters and types on which it depends.
	#[pallet::config]
	pub trait Config: frame_system::Config {
		/// Because this pallet emits events, it depends on the runtime's definition of an event.
		type Event: From<Event<Self>> + IsType<<Self as frame_system::Config>::Event>;
	}

	#[pallet::pallet]
	#[pallet::generate_store(pub(super) trait Store)]
	pub struct Pallet<T>(_);

	#[pallet::storage]
	#[pallet::getter(fn club_account_map)]
	pub(super) type ClubAccountMap<T: Config> = StorageDoubleMap<_, Identity, u32, Identity, T::AccountId, bool, ValueQuery>;

	#[pallet::event]
	#[pallet::generate_deposit(pub(super) fn deposit_event)]
	pub enum Event<T: Config> {
		ClubAdded(u32, T::AccountId),
	}

	#[pallet::error]
	pub enum Error<T> {
		NotClubMember,
	}

	// Dispatchable functions allows users to interact with the pallet and invoke state changes.
	// These functions materialize as "extrinsics", which are often compared to transactions.
	// Dispatchable functions must be annotated with a weight and must return a DispatchResult.
	#[pallet::call]
	impl<T: Config> Pallet<T> {
		#[pallet::weight(10_000 + T::DbWeight::get().writes(1))]
		pub fn add_club(origin: OriginFor<T>, club_id: u32, member: T::AccountId) -> DispatchResult {
			ensure_root(origin)?;
			ClubAccountMap::<T>::insert(club_id, &member, true);
			Self::deposit_event(Event::ClubAdded(club_id, member));
			Ok(())
		}

		#[pallet::weight(10_000 + T::DbWeight::get().writes(1))]
		pub fn rm_club(origin: OriginFor<T>, club_id: u32, member: T::AccountId) -> DispatchResult {
			ensure_root(origin)?;
			if ClubAccountMap::<T>::contains_key(club_id, &member) {
				ClubAccountMap::<T>::remove(club_id, member);
				Ok(())
			} else {
				Err(Error::<T>::NotClubMember)?
			}
		}
	}
}
